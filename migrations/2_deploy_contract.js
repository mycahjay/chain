var Chain = artifacts.require('./Chain.sol')

module.exports = function (deployer) {
  deployer.deploy(Chain, web3.toWei(0.1, 'ether'), { gas: 4700000 })
}
