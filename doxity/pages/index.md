# Deploy contract
_ensure metamask and ganache are linked_

## Configure _truffle.js_

```
require('babel-register')

module.exports = {
  networks: {
    development: {
      host: "localhost",
      from: 'ADDRESS_HERE', // get address from Ganache or Metamask
      port: 7545,
      network_id: "*"
    }
}
```

## Compile

```
> truffle compile

// Compiling ./contracts/Migrations.sol...
// Compiling ./contracts/Voting.sol...
// Writing artifacts to ./build/contracts
```

_build folder should now contain contracts folder with Migrations.json and Voting.json_

## Deploy

```
truffle deploy
```
