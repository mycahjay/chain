pragma solidity ^0.4.8;

contract ChainBase {
  /*** EVENTS ***/
  // Creation event that is triggered when a new chain is created.
  event Create(address indexed owner, uint256 tokenId, uint256 design);
  // Transfer event as defined in current draft of ERC721.
  // Emitted when ownership is assigned or chain is created.
  event Transfer(address indexed from, address indexed to, uint256 indexed tokenId);

    /*** DATA TYPES ***/
  struct Chain {
    // Owner of the piece.
    address owner;
    // Unique design of the piece.
    uint256 design;
    // Timestamp from the block when the piece was created.
    uint64 creationTime;
  }

  /*** STORAGE ***/
  // array containing the struct for all pieces that have been created.
  Chain[] chains;

  // Mapping from ChainIDs to the owner's address.
  mapping (uint256 => address) public tokenIndexToOwner;

  // Mapping from owner address to count of tokens owned by that address.
  mapping (address => uint256) ownershipTokenCount;

  // Mapping from ChainIDs to address that is approved to call transferFrom().
  // Each Chain can only have one approved address for transfer.
  mapping (uint256 => address) public tokenIndexToApproved;

  function _transfer(address _from, address _to, uint256 _tokenId) internal {
    ownershipTokenCount[_to]++;
    tokenIndexToOwner[_tokenId] = _to;

    if (_from != address(0)) {
      ownershipTokenCount[_from]--;
      delete tokenIndexToApproved[_tokenId];
    }

    Transfer(_from, _to, _tokenId);
  }

  // Creates chain token.
  function _createChain(address _owner) internal returns (uint) {
    uint256 design = uint256(_owner);
    Chain memory _chain = Chain({
      owner: _owner,
      design: design,
      creationTime: uint64(now)
    });
    uint256 newChainId = chains.push(_chain) - 1;
    // Ensures no more than 4 billion pieces are created.
    require(newChainId == uint256(uint32(newChainId)));

    // Emits the created event.
    Create(_owner, newChainId, design);
    // Assigns ownership and emits Transfer event as per ERC721 draft.
    _transfer(0, _owner, newChainId);
    return newChainId;
  }

  function createChain() external returns (uint256) {
    return _createChain(msg.sender);
  }

  // Returns chain token.
  function getToken(uint256 _tokenId) external view returns (address owner, uint256 design, uint64 creationTime) {
    Chain storage chain = chains[_tokenId];
    owner = chain.owner;
    design = chain.design;
    creationTime = chain.creationTime;
  }
}
