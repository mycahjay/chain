require('babel-register')

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      from: 'ADDRESS_HERE', // Get address from Ganache CLI /
      port: 8545,
      network_id: '*'
    },
    ropsten: {
      host: '127.0.0.1',
      port: 8545,
      gas: 4700000,
      network_id: 3
    }
  }
}
